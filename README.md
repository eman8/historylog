# Historique

Ceci est un fork du plugin https://github.com/UCSCLibrary/HistoryLog. 

Nous avons 

- traduit en français l'ensemble du plugin
- ajouté une version basique de l'historique pour les Simple Pages et les Exhibits (expositions).
- ajouté les éléments adéquats au filtre rapide
- simplification du tableau des logs

## Credits

Fork réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen) avec le soutien du projet Fiches de lecture Foucault : https://ffl.hypotheses.org. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/historique)
